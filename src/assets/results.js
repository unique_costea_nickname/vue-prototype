var ResultData = [
  {
    "_index": "watson_job_opening_search_1490876897",
    "_type": "job_opening",
    "_id": "198901",
    "_score": null,
    "_source": {
      "id": 198901,
      "title": "Per direct en voor langere tijd orderpickers gezocht voor de Lidl in Tiel",
      "short_desc": "Ben jij per direct beschikbaar op zaterdag en ben je daarnaast in de maanden juni, juli, augustus en september fulltime bereid om aan de slag te gaan? Is logistiek helemaal jouw ding? Dan zoeken wij jou! Voor het distributiecentrum van Lidl in Tiel zijn wij per direct op zoek naar orderpickers! Ben jij een harde, zelfstandige werker? Lees dan snel verder!",
      "long_desc": "<p>Lidl Tiel is per direct op zoek naar orderverzamelaars voor het weekend! Als orderverzamelaar bij Lidl zorg jij ervoor dat alle supermarkten op het juiste moment de juiste producten ontvangen. In drukke tijden gaan er per dag met gemak tienduizenden dozen de deur uit. Dus je moet zeker niet bang zijn om je handen uit de mouwen te steken! </p><p>Als orderverzamelaar ben je verantwoordelijk voor het verzamelen van de juiste producten in het distributiecentrum door middel van een voice picking systeem en zorg je ervoor dat alle pallets klaar zijn voor transport naar de winkels. Door jouw hoge werktempo krijgen de Lidl-filialen alle producten die zij hebben besteld op tijd binnen. Daarnaast werk je nauwkeurig en let je altijd op de kwaliteit van producten. Zo gaan er geen verkeerde of beschadigde producten mee naar de winkels. Als orderverzamelaar ben je dus onmisbaar in het logistieke proces!</p><p style=\"line-height: 22.4px;\"><strong>Wij bieden jou:</strong></p><ul><li>Een uitstekend salaris;</li><li>Een zeer goed inwerkproces;</li><li>Een zelfstandige bijbaan voor langere tijd; </li><li>Een persoonlijke mentor op de werkvloer;</li><li>De kans om relevante werkervaring op te doen bij een internationale organisatie.</li></ul><p><strong>Profiel:</strong></p><ul><li><strong></strong>Je bent 18 jaar of ouder;</li><li>Je bent een echte aanpakker en je kunt fysiek werk leveren;</li><li>Je werkt snel en nauwkeurig;</li><li>Je hebt een goede beheersing van de Nederlandse taal (je gaat werken met een voice picking-systeem);</li><li>Je hebt ruimtelijk inzicht;</li><li>Je woont in Tiel of directe omgeving.</li></ul><p style=\"line-height: 22.4px;\"><strong>Dagen en tijden:</strong></p><ul><li>Je bent standaard de zaterdag of de zondag beschikbaar;</li><li>Wil je meer werken? Dan kun je naast zaterdag of zondag ook op donderdag of vrijdag werken</li><li>Doordeweeks ben je beschikbaar op oproepbasis, bij ziekte of te kort kun jij extra uren maken;</li><li>Werktijden zijn van 8.30/9.00 uur tot 16.30/17.00 uur;</li></ul><p>Houd jij van aanpakken en vind je het leuk om in een team mee te werken aan het succes van Lidl? Reageer dan snel via de knop \"solliciteer direct!\"<br></p>",
      "desc_function_description": "<p class=\"MsoNormal\" style=\"margin-bottom: 11.25pt; line-height: normal; background-image: initial; background-color: white;\">Als orderpicker zorg jij dat alle supermarkten op het\r\njuiste moment de goede producten binnenkrijgen. Jij bent bereid om je handen\r\nuit de mouwen te steken want op drukke dagen gaan er per dag met gemak\r\ntienduizenden dozen de deur uit. Je bent dus ook stressbestendig en kunt goed\r\nsamenwerken met je collega’s. <o:p></o:p></p><p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt; line-height: normal; background-image: initial; background-color: white;\">Samen met hen ben je verantwoordelijk voor het\r\nverzamelen van de juiste producten in het distributiecentrum door middel van\r\neen voicepicking-systeem, en zorg je ervoor dat alle pallets klaar zijn voor\r\ntransport naar de winkels. Door jouw hoge werktempo krijgen de Lidl-filialen\r\nalle producten die zij hebben besteld op tijd binnen. Daarnaast werk je\r\nnauwkeurig en let je altijd op de kwaliteit van producten, die de deur uitgaan.\r\nDit zorgt ervoor dat er geen verkeerde of beschadigde producten in de winkels\r\nterechtkomen. <o:p></o:p></p><p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt; line-height: normal; background-image: initial; background-color: white;\"><o:p> </o:p></p><p>Wil jij onmisbaar zijn? Kun jij per direct aan de slag op zaterdag en ben je in de maanden\r\njuni, juli, augustus en september fulltime inzetbaar? Solliciteer dan vandaag\r\nnog!</p>",
      "desc_extra_info": "<p><strong>Beschikbaarheid:</strong><br></p><ul><li>Je bent per direct op zaterdag beschikbaar;</li><li>Je bent daarnaast fulltime beschikbaar in de maanden juni, juli, augustus en september;</li></ul>",
      "status_id": 3,
      "geo_type": 1,
      "location": "Tiel",
      "published_at": "2017-04-03T11:42:36.000+02:00",
      "is_scraped": false,
      "is_external": false,
      "is_national": false,
      "customer": {
        "id": 7333,
        "name": "YoungCapital vacatures"
      },
      "job_opening_site_members": [
        {
          "is_active": true,
          "site_id": 1
        },
        {
          "is_active": true,
          "site_id": 5
        },
        {
          "is_active": true,
          "site_id": 3
        },
        {
          "is_active": true,
          "site_id": 6
        },
        {
          "is_active": true,
          "site_id": 14
        },
        {
          "is_active": true,
          "site_id": 7
        },
        {
          "is_active": true,
          "site_id": 19
        },
        {
          "is_active": true,
          "site_id": 16
        }
      ],
      "functions": [
        {
          "id": 14,
					"name": "Transport / Logistiek / Chauffeur / Koerier"
        }
      ],
      "professions": [
        {
          "id": 103,
          "catchall": false,
          "function_id": 14
        }
      ],
      "job_categories": [
        {
          "id": 12
        }
      ],
      "job_types": [
        {
          "id": 2,
					"name": "Parttime"
        },
        {
          "id": 3,
					"name": "Avondwerk"
        },
        {
          "id": 4,
					"name": "Weekendwerk"
        }
      ],
      "edu_types": [
        {
          "id": 3,
					"name": "Weekendwerk"
        },
        {
          "id": 4,
					"name": "Weekendwerk"
        },
        {
          "id": 5,
					"name": "Weekendwerk"
        },
      ],
      "regions": [
        {
          "id": 28
        },
        {
          "id": 32
        }
      ],
      "locations": [
        {
          "lat": "51.912296",
          "lon": "5.452637"
        }
      ]
    },
    "sort": [
      0,
      0,
      1491212556000
    ]
  },
  {
    "_index": "watson_job_opening_search_1490876897",
    "_type": "job_opening",
    "_id": "199942",
    "_score": null,
    "_source": {
      "id": 199942,
      "title": "Zomerbaan? Word orderpicker in het distributiecentrum van Lidl in Tiel",
      "short_desc": "Heb jij in de zomermaanden nog geen plannen en kun jij dus lekker veel uren werken? Wil jij op de zaterdagen na de zomer blijven werken? Dan is dit een leuke logistieke baan voor jou! Ga aan de slag als orderpicker in het distributiecentrum van Lidl in Tiel. Lees snel verder voor meer informatie!",
      "desc_function_description": "<p>Heb jij flinke spierballen en ben\r\njij in een goede fysieke conditie? Dan is dit dé baan voor jou! Als orderpicker\r\nin het distributiecentrum van Lidl sjouw jij de hele dag met dozen. Jij zorgt er\r\nnamelijk voor dat alle supermarkten op tijd de juiste producten ontvangen. Je\r\nopdrachten ontvang je via het innovatieve voicepicking-systeem.</p><p class=\"MsoNormal\" style=\"margin: 7.5pt 0cm 15pt; line-height: normal; background-image: initial; background-color: white;\">Jij werkt snel en nauwkeurig en\r\nzorgt dat alle pallets op tijd klaar zijn voor transport. Natuurlijk let jij\r\nook op de kwaliteit van de producten. Zo gaan er geen verkeerde of beschadigde\r\nproducten mee naar de winkels. Jij krijgt dus een onmisbare taak in dit\r\ndistributiecentrum!<o:p></o:p></p><p>Heb jij in de maanden juni, juli,\r\naugustus en september nog geen plannen en kun jij fulltime aan de slag? Wil jij\r\ndaarnaast ook graag op de zaterdagen blijven werken na de zomer? Ga dan de uitdaging\r\naan om op een dag tienduizenden dozen te verwerken in het distributiecentrum\r\nvan Lidl! Solliciteer snel!</p>",
      "desc_extra_info": "<p><strong>Beschikbaarheid:</strong></p><ul><li>Je bent fulltime beschikbaar in de maanden juni, juli, augustus en september;</li><li>Je bent na de zomerperiode beschikbaar om op de zaterdagen te blijven werken;</li></ul>",
      "status_id": 3,
      "geo_type": 1,
      "location": "Tiel",
      "published_at": "2017-04-03T11:42:28.000+02:00",
      "is_scraped": false,
      "is_external": false,
      "is_national": false,
      "customer": {
        "id": 7333,
        "name": "YoungCapital vacatures"
      },
      "job_opening_site_members": [
        {
          "is_active": true,
          "site_id": 1
        },
        {
          "is_active": true,
          "site_id": 5
        },
        {
          "is_active": true,
          "site_id": 6
        },
        {
          "is_active": true,
          "site_id": 16
        }
      ],
      "functions": [
        {
          "id": 14,
					"name": "Transport / Logistiek / Chauffeur / Koerier"
        }
      ],
      "professions": [
        {
          "id": 103,
          "catchall": false,
          "function_id": 14
        }
      ],
      "job_categories": [
        {
          "id": 12
        }
      ],
      "job_types": [
        {
          "id": 1,
					"name": "Fulltime"
        },
        {
          "id": 5,
					"name": "Vakantiewerk"
        },
        {
          "id": 7,
					"name": "Ervaren"
        }
      ],
      "edu_types": [
        {
          "id": 3,
					"name": "Weekendwerk"
        },
        {
          "id": 4,
					"name": "Weekendwerk"
        },
        {
          "id": 5,
					"name": "Weekendwerk"
        },
      ],
      "regions": [
        {
          "id": 28
        },
        {
          "id": 32
        }
      ],
      "locations": [
        {
          "lat": "51.912296",
          "lon": "5.452637"
        }
      ]
    },
    "sort": [
      1,
      0,
      1491212548000
    ]
  },
	{
    "_index": "watson_job_opening_search_1490876897",
    "_type": "job_opening",
    "_id": "489532",
    "_score": null,
    "_source": {
      "id": 489532,
      "title": "Kickstart jouw carrière als fulltime klantadviseur bij DHL Express in Schiphol",
      "short_desc": "Ben jij op zoek naar de ideale startersfunctie waarin jouw ervaring met klantcontact goed van pas komt? Heb jij je hbo- of wo-diploma op zak, maar ben jij nog lang niet uitgeleerd? Dan is deze fulltime baan als klantadviseur bij DHL Express op Schiphol echt iets voor jou! Lees snel verder!",
      "desc_function_description": "<p class=\"MsoNormal\" style=\"margin: 3.75pt 0cm 7.5pt; line-height: normal; background-image: initial; background-color: white;\">Als klantadviseur ga jij aan de slag op de klantenservice-afdeling Electronic Shipping Solutions. Zoals de naam al doet vermoeden, houdt deze afdeling zich bezig met e-shipment diensten, oftewel geautomatiseerde zendingen. Jij adviseert zakelijke klanten welk systeem zij het beste kunnen gebruiken voor hun lokale en wereldwijze expres- en pakketzendingen. Tijdens de implementatie van het systeem bij de klant bied je telefonische ondersteuning, gebruikerstraining en beantwoord je alle vragen. Als het systeem eenmaal draait, houd je klanten op de hoogte van veranderingen. </p><p class=\"MsoNormal\" style=\"margin: 3.75pt 0cm 7.5pt; line-height: normal; background-image: initial; background-color: white;\">Om de klant zo goed mogelijk te helpen, schakel jij met buitendienst medewerkers, applicatiebeheerders en de technische support. naast het onderhouden van klantrelaties, wordt er ook van jou verwacht dat je meedenkt over het verbeteren van applicaties en processen. Je krijgt een hele uitdagende en veelzijdige functie, waarin jij de expert wordt die de klant optimaal bedient. Ben jij klaar om deze fulltime baan bij DHL Express op je cv te zetten? Solliciteer dan snel! </p>",
      "desc_extra_info": "",
      "status_id": 3,
      "geo_type": 1,
      "location": "Schiphol",
      "published_at": "2017-04-03T11:32:37.000+02:00",
      "is_scraped": false,
      "is_external": false,
      "is_national": false,
      "customer": {
        "id": 7333,
        "name": "YoungCapital vacatures"
      },
      "job_opening_site_members": [
        {
          "is_active": true,
          "site_id": 1
        },
        {
          "is_active": true,
          "site_id": 5
        }
      ],
      "functions": [
        {
          "id": 36,
					"name": "Callcenter / Customer Service"
        }
      ],
      "professions": [
        {
          "id": 34,
          "catchall": true,
          "function_id": 36
        }
      ],
      "job_categories": [],
      "job_types": [
        {
          "id": 1,
					"name": "Fulltime"
        },
        {
          "id": 7,
					"name": "Ervaren"
        }
      ],
      "edu_types": [
        {
          "id": 3,
					"name": "Weekendwerk"
        },
        {
          "id": 4,
					"name": "Weekendwerk"
        },
        {
          "id": 5,
					"name": "Weekendwerk"
        }
      ],
      "regions": [
        {
          "id": 69
        }
      ],
      "locations": [
        {
          "lat": "52.296447",
          "lon": "4.771219"
        }
      ]
    },
    "sort": [
      2,
      0,
      1491211957000
    ]
  },
  {
    "_index": "watson_job_opening_search_1490876897",
    "_type": "job_opening",
    "_id": "221562",
    "_score": null,
    "_source": {
      "id": 221562,
      "title": "Gratis je spierballen trainen bij grote webshopwinkel in Utrecht",
      "short_desc": "Op zoek naar een uitdagende en leuke bijbaan als bezorger in Utrecht? Voor een grote elektronica webshop zoeken wij een enthousiaste bijrijders! Wil jij iedere zaterdag en flexibel minimaal één dag doordeweeks werken? Dan ben jij degene die we zoeken! ",
      "desc_function_description": "<p>Voor één van de snelst groeiende webshops van de Benelux, zijn wij op zoek naar enthousiaste bijrijders. Als verhuizer ga je met een vaste chauffeur mee naar verschillende adressen in de regio Utrecht, voor het bezorgen van bestellingen. Het gaat voornamelijk om zware en grote producten. Het is daarom belangrijk dat je van sjouwen houdt! Je zorgt ervoor dat alle bestelde producten op tijd en op de juiste plek zijn. Ook is het belangrijk dat je in het bezit bent van een B-rijbewijs, zodat je in geval van nood het stuur kunt overnemen.</p><p><br><strong>Takenpakket:</strong></p><ul type=\"disc\">\r\n <li class=\"MsoNormal\">Je\r\n     laadt met een steekkar alle bestellingen in de bestelwagen.<o:p></o:p></li>\r\n <li class=\"MsoNormal\">Je\r\n     ondersteunt en assisteert je collega bij verkeershandelingen.</li><li class=\"MsoNormal\">Je\r\n     zorgt ervoor dat alle bestellingen op de juiste manier worden afgeleverd en\r\n     assisteert bij de installatie.<o:p></o:p></li>\r\n <li class=\"MsoNormal\">Je\r\n     neemt retouren mee terug vanaf onze afleverlocatie naar het magazijn.<o:p></o:p></li>\r\n</ul><p><iframe width=\"500\" height=\"281\" src=\"https://www.youtube.com/embed/MYZmbqBn3wQ\" frameborder=\"0\" allowfullscreen=\"\" style=\"background-color: initial;\"></iframe><br></p><span></span>",
      "desc_extra_info": "",
      "status_id": 3,
      "geo_type": 1,
      "location": "Utrecht",
      "published_at": "2017-04-03T11:42:12.000+02:00",
      "is_scraped": false,
      "is_external": false,
      "is_national": false,
      "customer": {
        "id": 4385,
        "name": "YoungCapital Logistieke vacatures"
      },
      "job_opening_site_members": [
        {
          "is_active": true,
          "site_id": 1
        },
        {
          "is_active": true,
          "site_id": 5
        },
        {
          "is_active": true,
          "site_id": 3
        },
        {
          "is_active": true,
          "site_id": 6
        },
        {
          "is_active": true,
          "site_id": 14
        },
        {
          "is_active": true,
          "site_id": 7
        },
        {
          "is_active": true,
          "site_id": 19
        }
      ],
      "functions": [
        {
          "id": 14,
					"name": "Transport / Logistiek / Chauffeur / Koerier"
        },
        {
          "id": 20,
					"name": "Productie"
        }
      ],
      "professions": [
        {
          "id": 14,
          "catchall": true,
          "function_id": 14
        },
        {
          "id": 20,
          "catchall": true,
          "function_id": 20
        }
      ],
      "job_categories": [
        {
          "id": 12
        }
      ],
      "job_types": [
        {
          "id": 2,
					"name": "Parttime"
        },
        {
          "id": 4,
					"name": "Weekendwerk"
        }
      ],
      "edu_types": [
        {
          "id": 4,
					"name": "Weekendwerk"
        },
        {
          "id": 5,
					"name": "Weekendwerk"
        }
      ],
      "regions": [
        {
          "id": 32
        }
      ],
      "locations": [
        {
          "lat": "52.112072",
          "lon": "5.05519"
        }
      ]
    },
    "sort": [
      3,
      0,
      1491212532000
    ]
  },
  {
    "_index": "watson_job_opening_search_1490876897",
    "_type": "job_opening",
    "_id": "284525",
    "_score": null,
    "_source": {
      "id": 284525,
      "title": "Aan de slag als bijrijder bij een super leuk bedrijf in Utrecht",
      "short_desc": "Ben jij de aanpakker die wil werken als bijrijder bij een superleuk bedrijf in Utrecht? Zorg jij ervoor dat de producten die vóór 23.59 zijn besteld de volgende dag bij de klant in huis staan? Kun je goed samenwerken, ben je iedere zaterdag beschikbaar en in goede fysieke conditie? Kom bij één van de snelst groeiende bedrijven van de Benelux werken!\r\n",
      "desc_function_description": "<ul type=\"disc\"><p class=\"MsoNormal\" style=\"margin-bottom:11.25pt;line-height:normal\">Voor één van de snelst groeiende webshops van de Benelux zijn wij op zoek\r\nnaar enthousiaste bijrijders.\r\nAls bijrijder ga je met een vaste chauffeur mee naar verschillende adressen in\r\nde regio Utrecht om bestellingen op de plaats van bestemming te bezorgen. Dit\r\nkan bijvoorbeeld op vier hoog zijn, in een dikke villa of een woonboerderij.\r\nHet kunnen grote en zware producten zijn waarmee je gaat werken, of het nu gaat\r\nom een televisie, een wasmachine of een vaatwasser, het is belangrijk dat je\r\nvan sjouwen houdt! Je zorgt ervoor dat de bestelde producten op tijd én op de\r\njuiste plek zijn. Jij maakt de belofte “voor 23:59 besteld is morgen in huis”\r\nwaar! Naast het leveren van de producten ga je samen met je collega de\r\napparaten installeren en testen. Ook is het belangrijk dat je in het bezit van\r\neen B-rijbewijs bent, zodat je in geval van nood het stuur kunt overnemen.<o:p></o:p></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-bottom:11.25pt;line-height:normal\"><strong>Zo ziet je dag eruit:</strong><o:p></o:p></p>\r\n\r\n<ul type=\"disc\">\r\n <li class=\"MsoNormal\">Je\r\n     begint om 6/7 uur 's ochtends met werken en bent om 2/3 uur weer klaar.<o:p></o:p></li>\r\n <li class=\"MsoNormal\">Je\r\n     laadt met een steekkar alle bestellingen in de bestelwagen.</li><li class=\"MsoNormal\">Je\r\n     ondersteunt en assisteert je collega bij verkeershandelingen.<o:p></o:p></li>\r\n <li class=\"MsoNormal\">Je\r\n     zorgt ervoor dat alle bestellingen op de juiste manier worden afgeleverd\r\n     en assisteert bij de installatie.<o:p></o:p></li>\r\n <li class=\"MsoNormal\">Je\r\n     neemt (oude) gebruikte apparaten mee terug vanaf de afleverlocatie naar\r\n     het magazijn.</li><li class=\"MsoNormal\">Je laat onze klanten met een grote glimlach achter.</li></ul>\r\n</ul><p class=\"MsoNormal\" style=\"margin-bottom: 6.75pt; line-height: normal; background-image: initial; background-color: white;\"><iframe width=\"500\" height=\"281\" src=\"https://www.youtube.com/embed/MYZmbqBn3wQ\" frameborder=\"0\" allowfullscreen=\"\"></iframe>\r\n<!--[if !supportLineBreakNewLine]--><br>\r\n<!--[endif]--><o:p></o:p></p>",
      "desc_extra_info": "",
      "status_id": 3,
      "geo_type": 1,
      "location": "Utrecht",
      "published_at": "2017-04-03T11:42:06.000+02:00",
      "is_scraped": false,
      "is_external": false,
      "is_national": false,
      "customer": {
        "id": 7297,
        "name": "YoungCapital Chauffeursdiensten vacatures"
      },
      "job_opening_site_members": [
        {
          "is_active": true,
          "site_id": 1
        },
        {
          "is_active": true,
          "site_id": 5
        },
        {
          "is_active": true,
          "site_id": 6
        }
      ],
      "functions": [
        {
          "id": 14,
					"name": "Transport / Logistiek / Chauffeur / Koerier"
        },
        {
          "id": 23,
					"name": "Technisch / Klusser / Monteur"
        }
      ],
      "professions": [
        {
          "id": 23,
          "catchall": true,
          "function_id": 23
        },
        {
          "id": 94,
          "catchall": false,
          "function_id": 14
        },
        {
          "id": 100,
          "catchall": false,
          "function_id": 14
        }
      ],
      "job_categories": [
        {
          "id": 12
        }
      ],
      "job_types": [
        {
          "id": 1,
					"name": "Fulltime"
        },
        {
          "id": 4,
					"name": "Weekendwerk"
        },
        {
          "id": 7,
					"name": "Ervaren"
        }
      ],
      "edu_types": [
        {
          "id": 3,
					"name": "Weekendwerk"
        },
        {
          "id": 4,
					"name": "Weekendwerk"
        },
        {
          "id": 5,
					"name": "Weekendwerk"
        }
      ],
      "regions": [
        {
          "id": 32
        }
      ],
      "locations": [
        {
          "lat": "52.112072",
          "lon": "5.05519"
        }
      ]
    },
    "sort": [
      4,
      0,
      1491212526000
    ]
  },
  {
    "_index": "watson_job_opening_search_1490876897",
    "_type": "job_opening",
    "_id": "192234",
    "_score": null,
    "_source": {
      "id": 192234,
      "title": "Goed verdienen als parttime klantenservicemedewerker bij Lidl ",
      "short_desc": "Wil jij lekker verdienen in een flexibele parttime functie? Ben jij klantvriendelijk en heb je altijd een antwoord klaar? Zit jij graag op social media en wil je werken voor een groot internationaal bedrijf waar je kunt doorgroeien? Check dan zeker deze vacature!",
      "long_desc": "<p>Door\r\nhet succes heeft dit bedrijf door uitbreiding van de dienstverlening bij customer\r\ncare een vacature van medewerker klantenservice openstaan. Het is jouw\r\ntaak om alle vragen en klachten naar tevredenheid af te handelen en de\r\nklanttevredenheid hoog te houden. Jouw ervaring van minimaal één jaar op een\r\ntelefonische klantenservice heeft jouw enthousiast gemaakt voor het vak. Op deze kleine afdeling zie je alles\r\nvoorbij komen en heb je de mogelijkheid om actief mee te denken in verbeteringen van bedrijfsprocessen. </p><p class=\"MsoNormal\"><strong>Wat houdt de baan in?<o:p></o:p></strong></p><ul type=\"disc\"><li>Dagelijkse verantwoordelijkheden voor het\r\nafhandelen van vragen van onze klanten via telefoon, e-mails, brieven en social media;</li><li>Je behandelt de vragen en klachten met de grootst mogelijke zorg en streeft daarmee\r\nhoge klant tevredenheid na;</li><li>Je probeert de vragen zo veel mogelijk zelf af te handelen en als het\r\nnodig is, zet je de vragen of klachten door naar de relevante afdeling binnen het bedrijf. Je behoudt\r\nin dat geval wel de regie over het proces en het beantwoorden van de vraag;</li><li>Je voert dit allemaal uit volgens de interne richtlijnen die voor de klantenservice zijn opgesteld;</li><li>Wanneer je verbeterpunten ziet om de werkwijze van de klantenservice\r\nte optimaliseren, dan laat je het niet na om dit aan te geven en verder uit te\r\nwerken;</li>\r\n <li class=\"MsoNormalCxSpFirst\">Jij bent pas tevreden als onze klanten dit ook\r\n     zijn!</li></ul><p><strong>Wat vragen wij van jouw?</strong></p><ul><li><strong>Minimaal\r\néén jaar</strong> werkervaring bij een telefonische of online klantenservice;</li><li>Je beschikt\r\nover hbo werk- en denkniveau;</li><li>De\r\nNederlandse taal beheers je zowel mondeling als schriftelijk erg goed. Heb je\r\nook kennis van de Duitse taal dan is dat een pré;</li><li>Je kunt kritisch kijken naar bestaande\r\nprocessen en verbetervoorstellen doen en implementeren;</li><li>Omdat\r\nje altijd rustig en klantvriendelijk blijft, ben je een prettig en begripvol\r\naanspreekpunt voor onze klanten;</li><li>Daarnaast\r\nbeschik je over een groot verantwoordelijkheidsgevoel en flexibele instelling\r\nom onze klanten zo goed mogelijk van dienst te zijn;</li><li>Je bent\r\nminimaal 24 uur per week beschikbaar en minimaal een zaterdag en zondag\r\nper maand beschikbaar voor onze openingstijden in het weekend. Je hebt\r\nwisselende werktijden tussen 8:00 en 21:00 doordeweeks en tussen 10:00 en 18:00\r\nik het weekend;</li><li>Je\r\nwoont binnen een straal van 25 km van Huizen.</li></ul><p> <strong>Wij bieden jouw:</strong></p><ul><li>Een\r\nuitdagende en vernieuwde baan waarbij o.a. social media onderdeel is van onze\r\ncustomer care afdeling;</li><li>De\r\nmogelijkheid om in een klein team je te verdiepen en te verbreden in de klantenservice;</li><li>Een bedrijf\r\ndat volop in ontwikkeling is;</li><li>Een\r\nmarktconform salaris en goede arbeidsvoorwaarden;</li></ul><p>Ben jij geïnteresseerd in deze vacature en kun je niet wachten aan de slag te gaan? Solliciteer direct online via de knop ‘Solliciteer direct!’</p>",
      "desc_function_description": "<p>Je gaat werken bij Lidl in het Gooi. Als\r\nklantenservicemedewerker ben jij degene die de klanten blij en tevreden houdt.\r\nJe beantwoordt al hun brandende vragen en staat voor ze klaar om advies te\r\ngeven. Dit doe je niet alleen telefonisch en via e-mail, maar ook via social\r\nmedia zoals Twitter. Je gaat werken in een leuk klein team vol gedreven en\r\ngezellige collega’s. Samen met je team denk je actief mee over wat er nóg beter\r\nkan in het bedrijf. Er is dus ook ruimte voor eigen inbreng! Binnen het bedrijf\r\nzijn genoeg doorgroeimogelijkheden.!</p>",
      "desc_extra_info": "",
      "status_id": 3,
      "geo_type": 2,
      "location": "",
      "published_at": "2017-04-03T11:38:57.000+02:00",
      "is_scraped": false,
      "is_external": false,
      "is_national": false,
      "customer": {
        "id": 7564,
        "name": "YoungCapital CallCenter vacatures"
      },
      "job_opening_site_members": [
        {
          "is_active": true,
          "site_id": 1
        },
        {
          "is_active": true,
          "site_id": 5
        },
        {
          "is_active": true,
          "site_id": 3
        },
        {
          "is_active": true,
          "site_id": 6
        },
        {
          "is_active": true,
          "site_id": 14
        },
        {
          "is_active": true,
          "site_id": 7
        },
        {
          "is_active": true,
          "site_id": 16
        },
        {
          "is_active": true,
          "site_id": 19
        }
      ],
      "functions": [
        {
          "id": 6,
					"name": "Communicatie / Marketing / Reclame / PR"
        },
        {
          "id": 36,
					"name": "Callcenter / Customer Service"
        }
      ],
      "professions": [
        {
          "id": 6,
          "catchall": true,
          "function_id": 6
        },
        {
          "id": 43,
          "catchall": false,
          "function_id": 36
        }
      ],
      "job_categories": [
        {
          "id": 12
        }
      ],
      "job_types": [
        {
          "id": 1,
					"name": "Fulltime"
        },
        {
          "id": 2,
					"name": "Parttime"
        },
        {
          "id": 3,
					"name": "Avondwerk"
        },
        {
          "id": 4,
					"name": "Weekendwerk"
        },
        {
          "id": 8,
					"name": "Tijdelijke fulltime baan"
        }
      ],
      "edu_types": [
        {
          "id": 1,
					"name": "Weekendwerk"
        },
        {
          "id": 2,
					"name": "Weekendwerk"
        },
        {
          "id": 3,
					"name": "Weekendwerk"
        },
        {
          "id": 4,
					"name": "Weekendwerk"
        },
        {
          "id": 5,
					"name": "Weekendwerk"
        }
      ],
      "regions": [
        {
          "id": 2
        },
        {
          "id": 26
        },
        {
          "id": 45
        },
        {
          "id": 47
        },
        {
          "id": 55
        },
        {
          "id": 69
        }
      ],
      "locations": [
        {
          "lat": "52.15427039",
          "lon": "5.3877115"
        },
        {
          "lat": "52.5319726",
          "lon": "5.5188672"
        },
        {
          "lat": "52.41681229",
          "lon": "5.22054952"
        },
        {
          "lat": "52.21347337",
          "lon": "5.2995918"
        },
        {
          "lat": "52.23048377",
          "lon": "5.1764634"
        },
        {
          "lat": "52.28",
          "lon": "4.756667"
        }
      ]
    },
    "sort": [
      5,
      0,
      1491212337000
    ]
  },
  {
    "_index": "watson_job_opening_search_1490876897",
    "_type": "job_opening",
    "_id": "195536",
    "_score": null,
    "_source": {
      "id": 195536,
      "title": "Commerciële uitdaging voor 6 maanden met relocatie naar Bulgarije!",
      "short_desc": "Heb je altijd al in het buitenland willen werken en ben jij commercieel ingesteld? Dan heb je geluk! Een groot klant contact center in Bulgarije is op zoek naar ambitieuze medewerkers! Jij krijgt de mogelijkheid om minimaal 6 maanden in de prachtige hoofdstad Sofia te gaan werken en jezelf nóg verder te ontwikkelen. Dé start van jouw (internationale) carrière in de Sales! Bovendien wordt jouw vliegticket geheel vergoed en krijg je goede begeleiding in het vinden van woonruimte. Ga jij het avontuur aan? Lees dan snel verder! \r\n \r\n",
      "long_desc": "<p>Jij gaat aan de slag bij een grote serviceorganisatie die is gevestigd in Sofia, Bulgarije. Als Outbound Sales Medewerker benader jij bestaande klanten van een internationaal bedrijf gespecialiseerd in diepvriesproducten. Met jouw sales talent en inlevingsvermogen, weet jij iedere klant enthousiast te maken over de producten en de diensten van het bedrijf. Jij voelt precies aan wat de wensen van de klant zijn en speelt hier slim op in. De klant is blij met jouw goede kennis en plaatst graag een nieuwe bestelling bij je vanwege jouw benadering. En jij? Jij hebt er weer een tevreden klant én een mooie bonus bij! </p><p>Jij bent diegene die het initiatief neemt en door middel van een professionele aanpak het klantencontact tot een vruchtbaar einde brengt.</p><ul><li>Je maakt afspraken met prospects voor vertegenwoordigers (agendabeheer).</li><li>Je belt klanten op, gaat hun gegevens na en past ze zo nodig aan (databasebeheer).</li><li>Je belt prospects/klanten op voor het afnemen van een enquête.</li><li>Je contacteert prospects/klanten en biedt hen een product of dienst aan (sales).</li></ul><p>Je komt te werken in een informeel en internationaal team. Er is veel aandacht voor persoonlijke groei en ontwikkeling. Daarnaast word je goed begeleid met het vinden van woonruimte en het regelen van de lokale administratie. Bovendien wordt jouw ticket naar Bulgarije voor je geregeld én vergoed. </p><p><br><strong>Wat wij bieden: </strong></p><ul><li>Een marktconform salaris van €1007,- netto. In Bulgarije is het leven super goedkoop. Zo kun je al uit eten voor €6,- per maaltijd!</li><li> De eerste 2 dagen in Bulgarije krijg je een hotelaccommodatie aangeboden. </li><li> Een aantrekkelijke bonusregeling.<span></span></li><li><span></span> Internationale werkervaring opdoen bij een informeel bedrijf.</li><li> De mogelijkheid om in de mooie stad Sofia te werken.</li><li> Uitgebreide doorgroeimogelijkheden. </li><li><span></span> Direct een contract bij onze opdrachtgever!</li></ul><p><br><strong>Wat wij vragen: </strong></p><ul><li>Je hebt minimaal mbo werk- en denkniveau.</li><li><span></span> Je bent bereid om in het buitenland te werken en wonen. </li><li> Je beschikt over uitstekende communicatieve vaardigheden, daarbij is de beheersing van de Nederlandse taal  een must. </li><li><span></span>Je bent fulltime en flexibel inzetbaar</li><li>Je bent minimaal 6 maanden beschikbaar (langer is zeker mogelijk). </li></ul>* Bulgarije behoort tot de Europese Unie, je hoeft dus geen visum of internationale reispas te hebben. <p><br><br>Ben je geïnteresseerd in deze internationale baan functie in Bulgarije? Klik op de knop '<strong>Solliciteer direct!'</strong> en voeg je recente cv toe. Wij zullen dan zo snel mogelijk contact met je opnemen. Heb je vragen over deze vacature? Bel dan naar 023-2052521 en vraag naar Esther van Rooijen.</p>",
      "desc_function_description": "<p>Jij gaat aan de slag bij een grote serviceorganisatie die is gevestigd in Sofia, Bulgarije. Als Outbound Sales Medewerker benader jij bestaande klanten van een internationaal bedrijf gespecialiseerd in diepvriesproducten. Met jouw sales talent en inlevingsvermogen, weet jij iedere klant enthousiast te maken over de producten en de diensten van het bedrijf. Jij voelt precies aan wat de wensen van de klant zijn en speelt hier slim op in. De klant is blij met jouw goede kennis en plaatst graag een nieuwe bestelling bij je vanwege jouw benadering. En jij? Jij hebt er weer een tevreden klant én een mooie bonus bij!</p><p>Jij bent diegene die het initiatief neemt en door middel van een professionele aanpak het klantencontact tot een vruchtbaar einde brengt.</p><ul><li>Je maakt afspraken met prospects voor vertegenwoordigers (agendabeheer).</li><li>Je belt klanten op, gaat hun gegevens na en past ze zo nodig aan (databasebeheer).</li><li>Je belt prospects/klanten op voor het afnemen van een enquête.</li><li>Je contacteert prospects/klanten en biedt hen een product of dienst aan (sales).</li></ul><p>Je komt te werken in een informeel en internationaal team. Er is veel aandacht voor persoonlijke groei en ontwikkeling. Daarnaast word je goed begeleid met het vinden van woonruimte en het regelen van de lokale administratie. Bovendien wordt jouw ticket naar Bulgarije voor je geregeld én vergoed.</p>",
      "desc_extra_info": "",
      "status_id": 3,
      "geo_type": 4,
      "location": "Amsterdam",
      "published_at": "2017-04-03T11:32:47.000+02:00",
      "is_scraped": false,
      "is_external": false,
      "is_national": false,
      "customer": {
        "id": 7564,
        "name": "YoungCapital CallCenter vacatures"
      },
      "job_opening_site_members": [
        {
          "is_active": true,
          "site_id": 1
        },
        {
          "is_active": true,
          "site_id": 5
        },
        {
          "is_active": true,
          "site_id": 6
        }
      ],
      "functions": [
        {
          "id": 33,
					"name": "Commercieel / Verkoop / Inkoop"
        },
        {
          "id": 36,
					"name": "Callcenter / Customer Service"
        }
      ],
      "professions": [
        {
          "id": 42,
          "catchall": false,
          "function_id": 36
        },
        {
          "id": 43,
          "catchall": false,
          "function_id": 36
        },
        {
          "id": 46,
          "catchall": false,
          "function_id": 33
        }
      ],
      "job_categories": [],
      "job_types": [
        {
          "id": 1,
					"name": "Fulltime"
        },
        {
          "id": 8,
					"name": "Tijdelijke fulltime baan"
        }
      ],
      "edu_types": [
        {
          "id": 3,
					"name": "Weekendwerk"
        },
        {
          "id": 4,
					"name": "Weekendwerk"
        },
        {
          "id": 5,
					"name": "Weekendwerk"
        }
      ],
      "regions": [
        {
          "id": 41
        }
      ],
      "locations": []
    },
    "sort": [
      6,
      0,
      1491211967000
    ]
  },
  {
    "_index": "watson_job_opening_search_1490876897",
    "_type": "job_opening",
    "_id": "180019",
    "_score": null,
    "_source": {
      "id": 180019,
      "title": "Internationale ervaring opdoen met relocatie naar Athene, Griekenland ",
      "short_desc": "Een internationale werkervaring is een must en onvergetelijke ervaring voor iedereen met ambitie. YoungCapital biedt jou de mogelijkheid deze op te doen bij een wereldwijd facilitair callcenter in hartje Athene!",
      "long_desc": "<p><strong>Het werk <br></strong>In Athene ga je als Nederlandstalige klantenservice medewerker aan het werk en werk je voor een grote internationaal bekend mediabedrijf. Zowel telefonisch als per e-mail sta je Nederlandse klanten te woord over vragen omtrent: bestellingen, adreswijzigingen of contracten. Je geeft ondersteuning aan particuliere klanten en retailers van het bedrijf. Ook behandel je vragen met betrekking tot verschillende gebruiksproblemen van producten, vandaar dat licht technische kennis een vereiste is.</p><p><strong>Vlucht\r\n</strong><br>De vlucht naar Athene dient voorgeschoten te worden en deze krijg je na 9 maanden volledig terugbetaald door je werkgever. Bij aankomst staat het welkomstteam voor je klaar. Door hen word je direct wegwijs gemaakt in waar je werklocatie is.<br>\r\n<br><strong>Huisvesting\r\n</strong><br>Bij aankomst word je gekoppeld aan een makelaar die jou begeleidt in de zoektocht naar een fijne accommodatie in de buurt van de werklocatie. Ook voor vele andere vragen kun je bij hem of haar terecht. Tot die tijd is er voor 2 weken een hotel voor je geregeld dat ook volledig wordt vergoed.<br>\r\n<br><strong>Contract, Training en Inwerken\r\n</strong><br>Bij aanname ontvang je een jaarcontract bij onze opdrachtgever. Je start met een 2 weekse opleiding die wordt afgesloten met een toets. Eenmaal aan het werk kun je leiderschapscursussen en Griekse lessen volgen. Daarnaast ben je vrij om deel te nemen aan de verschillende activiteiten en de inhouse sportfaciliteiten. <br>\r\n<br><strong>Wonen in Griekenland en de Griekse cultuur\r\n</strong><br>Naast je werk heb je genoeg vrije tijd om samen met (Nederlandstalige) collega’s de omgeving te ontdekken. Dagelijks ervaar je de Zuid-Europese cultuur. De voordelen en natuurlijk ook de nadelen. Kortom, een unieke kans om werkervaring op te doen in het buitenland!<br></p><p><strong>Salaris\r\n</strong><br>Het bruto maandsalaris exclusief huisvesting bedraagt €1100,-<br><br></p><p><strong>Waarom via YoungCapital aan het werk?:</strong><br></p><ul><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Gratis vliegticket! </span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Taxikosten van vliegveld naar hotel worden vergoed.</span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Hotelovernachting tot 2 weken geregeld en vergoed. </span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Ondersteuning bij het regelen van jouw appartement. </span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Via werkgever (gevestigd over de hele wereld) verzekerd, gratis en betrouwbaar. </span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Ondersteuning bij een goede ‘work-life balance’. </span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Gratis Griekse lessen om de taal te leren. </span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Een mooie en zonnige locatie. </span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Werkzaamheden vinden plaats binnen een wereldbekend contactcenter. </span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Uitgebreide betaalde trainingen. </span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Duidelijke doorgroeimogelijkheden. </span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Goed salaris en een leuke bonusstructuur. </span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Een team collega's werkzaam die jou de weg wijs maakt en je je snel thuis laat voelen in je nieuwe omgeving.</span></li></ul><p><strong>Wat vragen wij van jou: </strong><br></p><ul><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Goede beheersing van de Nederlandse en Engelse taal en je bent communicatief sterk; </span></li><li>Je hebt affiniteit met de techniek aangezien je licht technische vragen dient te behandelen; </li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Je hebt minimaal een afgeronde MBO opleiding; </span></li><li><span style=\"font-size: 12px; line-height: 1em; background-color: initial;\">Je kan informatie makkelijk verwerken en je werkt gemakkelijk samen;<br> </span></li></ul><p><strong>Beschikbaarheid: \r\n</strong><br>Je start met een fulltime training waarbij je wordt klaargestoomd om zelfstandig aan de slag  te gaan (deze is in het Engels). Deze betaalde training start 16 januari in Athene! <br>\r\n<br>Ben jij de avonturier die wij zoeken en kun je haast niet wachten om je koffer in te pakken? Wacht dan niet langer en reageer snel via de ‘<strong>Solliciteer direct</strong>’ knop of bel ons gerust via 023-2052521 of mail naar e.vanrooijen@youngcapital.nl.<br></p>",
      "desc_function_description": "<p><strong>Het werk<br></strong>In Athene ga je als Nederlandstalige klantenservice medewerker aan het werk en werk je voor een grote internationaal bekend mediabedrijf. Zowel telefonisch als per e-mail sta je Nederlandse klanten te woord over vragen omtrent: bestellingen, adreswijzigingen of contracten. Je geeft ondersteuning aan particuliere klanten en retailers van het bedrijf. Ook behandel je vragen met betrekking tot verschillende gebruiksproblemen van producten, vandaar dat licht technische kennis een vereiste is.</p><p><strong>Huisvesting </strong><br>Bij aankomst word je gekoppeld aan een makelaar die jou begeleidt in de zoektocht naar een fijne accommodatie in de buurt van de werklocatie. Ook voor vele andere vragen kun je bij hem of haar terecht. Tot die tijd is er voor 2 weken een hotel voor je geregeld dat ook volledig wordt vergoed.<br><br><strong>Contract, Training en Inwerken </strong><br>Bij aanname ontvang je een jaarcontract bij onze opdrachtgever. Je start met een 2 weekse opleiding die wordt afgesloten met een toets. Eenmaal aan het werk kun je leiderschapscursussen en Griekse lessen volgen. Daarnaast ben je vrij om deel te nemen aan de verschillende activiteiten en de inhouse sportfaciliteiten. <br><br><strong>Wonen in Griekenland en de Griekse cultuur </strong><br>Naast je werk heb je genoeg vrije tijd om samen met (Nederlandstalige) collega’s de omgeving te ontdekken. Dagelijks ervaar je de Zuid-Europese cultuur. De voordelen en natuurlijk ook de nadelen. Kortom, een unieke kans om werkervaring op te doen in het buitenland!</p>",
      "desc_extra_info": "",
      "status_id": 3,
      "geo_type": 4,
      "location": "Amsterdam",
      "published_at": "2017-04-03T11:32:42.000+02:00",
      "is_scraped": false,
      "is_external": false,
      "is_national": false,
      "customer": {
        "id": 7564,
        "name": "YoungCapital CallCenter vacatures"
      },
      "job_opening_site_members": [
        {
          "is_active": true,
          "site_id": 1
        },
        {
          "is_active": true,
          "site_id": 5
        },
        {
          "is_active": true,
          "site_id": 6
        }
      ],
      "functions": [
        {
          "id": 36,
					"name": "Callcenter / Customer Service"
        }
      ],
      "professions": [
        {
          "id": 43,
          "catchall": false,
          "function_id": 36
        },
        {
          "id": 44,
          "catchall": false,
          "function_id": 36
        }
      ],
      "job_categories": [],
      "job_types": [
        {
          "id": 1,
					"name": "Fulltime"
        },
        {
          "id": 7,
					"name": "Ervaren"
        },
        {
          "id": 8,
					"name": "Tijdelijke fulltime baan"
        }
      ],
      "edu_types": [
        {
          "id": 4,
					"name": "Weekendwerk"
        },
        {
          "id": 5,
					"name": "Weekendwerk"
        }
      ],
      "regions": [
        {
          "id": 41
        }
      ],
      "locations": []
    },
    "sort": [
      7,
      0,
      1491211962000
    ]
  },
  {
    "_index": "watson_job_opening_search_1490876897",
    "_type": "job_opening",
    "_id": "489290",
    "_score": null,
    "_source": {
      "id": 489290,
      "title": "Fulltime facilitaire topper gezocht voor een internationale bank in Utrecht!",
      "short_desc": "Heb jij basiskennis van serveren en heb jij ervaring in kwaliteits-horeca (vergaderservices of gastheer/gastvrouw bij grote evenementen)? Heb jij inzicht in de voorschriften op het gebied van veiligheid en sociale hygiëne en heb jij bij voorkeur kennis van de bedrijfscultuur van de Rabobank? Dan zijn wij op zoek naar jou!\r\n\r\n",
      "desc_function_description": "<p>Rabobank Nederland afdeling VIP Service is per direct op zoek naar een professionele medewerker bediening op de diverse VIP gerelateerde afdelingen van het hoofdkantoor.</p><strong>Werkzaamheden: </strong><br style=\"color: rgb(0, 0, 0); font-family: &quot;arial regular&quot;, arial, sans-serif; font-size: 13px; margin: 0px; padding: 0px;\"><ul><li>Zorg dragen voor alle voorkomende bedieningswerkzaamheden.</li><li>Dienst verlenen in het algemeen en het direct/proactief reageren op de verwachtingen en wensen van gasten. </li><li>Je bent primair verantwoordelijk voor het serveren van dranken en gerechten, maar je kan ook worden ingezet op andere VIP gerelateerde werkzaamheden. <span></span></li></ul>",
      "desc_extra_info": "<p>Als facilitair medewerker zal jij worden ingezet in één van de twee diensten:</p><ul><li>Ochtenddienst van 07:00-15:30 uur</li><li>Middagdienst van 11:00-21:00 uur</li></ul><p><span></span></p>",
      "status_id": 3,
      "geo_type": 1,
      "location": "Utrecht",
      "published_at": "2017-04-03T11:19:09.000+02:00",
      "is_scraped": false,
      "is_external": false,
      "is_national": false,
      "customer": {
        "id": 6887,
        "name": "YoungCapital Horeca / Catering vacatures"
      },
      "job_opening_site_members": [
        {
          "is_active": true,
          "site_id": 1
        },
        {
          "is_active": true,
          "site_id": 5
        },
        {
          "is_active": true,
          "site_id": 6
        }
      ],
      "functions": [
        {
          "id": 10,
					"name": "Horeca / Catering"
        },
        {
          "id": 37,
					"name": "Schoonmaak / Facilitair"
        }
      ],
      "professions": [
        {
          "id": 10,
          "catchall": true,
          "function_id": 10
        },
        {
          "id": 35,
          "catchall": true,
          "function_id": 37
        },
        {
          "id": 57,
          "catchall": false,
          "function_id": 10
        },
        {
          "id": 93,
          "catchall": false,
          "function_id": 37
        }
      ],
      "job_categories": [],
      "job_types": [
        {
          "id": 7,
					"name": "Ervaren"
        },
        {
          "id": 8,
					"name": "Tijdelijke fulltime baan"
        }
      ],
      "edu_types": [
        {
          "id": 4,
					"name": "Weekendwerk"
        },
        {
          "id": 5,
					"name": "Weekendwerk"
        }
      ],
      "regions": [
        {
          "id": 32
        }
      ],
      "locations": [
        {
          "lat": "52.085467",
          "lon": "5.109817"
        }
      ]
    },
    "sort": [
      8,
      0,
      1491211149000
    ]
  },
  {
    "_index": "watson_job_opening_search_1490876897",
    "_type": "job_opening",
    "_id": "216220",
    "_score": null,
    "_source": {
      "id": 216220,
      "title": "Fulltime orderverzamelaar distributiecentrum Lidl Veghel ",
      "short_desc": "Ben jij op zoek naar een fulltime baan in de productie. Dan is dit je kans. Voor het distributiecentrum van Lidl in Veghel zijn we op zoek naar een enthousiaste order sorteerder. Een baan van maandag tot en met vrijdag van ongeveer 06:30-15:30. En je verdient goed!! Vanaf 23 jaar verdien je €12,31 per uur!! \r\n\r\n\r\n",
      "desc_function_description": "<p>We zijn op zoek naar een fulltime orderverzamelaar die minimaal tot september beschikbaar is. Ben je langer beschikbaar, dan kun je uiteraard ook solliciteren. </p><p>Je gaat binnen deze functie in het magazijn van Lidl aan de slag als orderverzamelaar. Het is afwisselend werk, waarbij je je zeker niet hoeft te vervelen. Het is de bedoeling dat je binnenkomende artikelen uitpakt en scant om ervoor te zorgen dat de voorraadinformatie up to date is. Vervolgens moeten alle artikelen die op de lopende band langskomen gesorteerd worden en worden opgeslagen in dozen. De laatste stap is de artikelen voorzien van een sticker. Kortom: jij zorgt dat het uit- en inpakproces soepel verloopt!</p><p>Spreekt deze vacature jou aan en kun je niet wachten om aan de slag te gaan? Wij zien je sollicitatie graag tegemoet!</p>",
      "desc_extra_info": "<p>Let op: dit is een poolvacature!</p>",
      "status_id": 3,
      "geo_type": 2,
      "location": "Veghel",
      "published_at": "2017-04-03T11:17:14.000+02:00",
      "is_scraped": false,
      "is_external": false,
      "is_national": false,
      "customer": {
        "id": 7285,
        "name": "YoungCapital Productie vacatures"
      },
      "job_opening_site_members": [
        {
          "is_active": true,
          "site_id": 1
        },
        {
          "is_active": true,
          "site_id": 5
        },
        {
          "is_active": false,
          "site_id": 3
        },
        {
          "is_active": true,
          "site_id": 6
        }
      ],
      "functions": [
        {
          "id": 14,
					"name": "Transport / Logistiek / Chauffeur / Koerier"
        },
        {
          "id": 20,
					"name": "Productie"
        }
      ],
      "professions": [
        {
          "id": 85,
          "catchall": false,
          "function_id": 20
        },
        {
          "id": 99,
          "catchall": false,
          "function_id": 14
        }
      ],
      "job_categories": [],
      "job_types": [
        {
          "id": 1,
					"name": "Fulltime"
        },
        {
          "id": 7,
					"name": "Ervaren"
        }
      ],
      "edu_types": [
        {
          "id": 4,
					"name": "Weekendwerk"
        },
        {
          "id": 5,
					"name": "Weekendwerk"
        }
      ],
      "regions": [
        {
          "id": 10
        },
        {
          "id": 14
        },
        {
          "id": 23
        },
        {
          "id": 67
        },
        {
          "id": 73
        }
      ],
      "locations": [
        {
          "lat": "51.68939625",
          "lon": "5.29530087"
        },
        {
          "lat": "51.44199628",
          "lon": "5.48133944"
        },
        {
          "lat": "51.48616105",
          "lon": "5.66164986"
        },
        {
          "lat": "51.66271702",
          "lon": "5.60530533"
        },
        {
          "lat": "51.77285249",
          "lon": "5.52506766"
        }
      ]
    },
    "sort": [
      9,
      0,
      1491211034000
    ]
  }
]

export default ResultData;
