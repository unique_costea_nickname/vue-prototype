import Vue from 'vue'
import Router from 'vue-router'
import List from '@/components/results/List';
import MapSearch from '@/components/map/MapSearch';

Vue.use(Router)

export default new Router({
	mode: 'history',
	linkActiveClass: 'active-page',
  routes: [
		{
			path: '/',
			redirect: '/list'
		},
    {
      path: '/list',
      name: 'searchPage',
      component: List
    },
		{
			path: '/map',
			name: 'mapSearch',
			component: MapSearch
		}
  ]
})
